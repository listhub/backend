import { Prisma, List, Tag } from '@prisma/client';
import { prisma } from '../index';

class TagService {
    async createTag(tagData: Prisma.TagCreateInput): Promise<Tag> {
        return prisma.tag.create({ data: tagData });
    }

    async getTagById(tagId: string): Promise<Tag | null> {
        return prisma.tag.findUnique({ where: { id: tagId } });
    }

    async getAllTags(): Promise<Tag[]> {
        return prisma.tag.findMany({
            include: {
                lists: true
            }
        });
    }

    async updateTag(tagId: string, tagData: Prisma.TagUpdateInput): Promise<Tag | null> {
        return prisma.tag.update({ where: { id: tagId }, data: tagData });
    }

    async deleteTag(tagId: string): Promise<Tag | null> {
        return prisma.tag.delete({ where: { id: tagId } });
    }

    async getListsByTagId(tagId: string): Promise<List[]> {
        const tag = await prisma.tag.findUnique({
            where: { id: tagId },
            include: { lists: true }
        });

        if (!tag) {
            throw new Error(`Tag with id ${tagId} not found.`);
        }

        return tag.lists;
    }

};

export default new TagService();