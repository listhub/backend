import { List, Prisma, Tag } from "@prisma/client";
import { prisma } from "..";

class ListService {

    async getListRecursive(id: string) {
        const listWithChildren = await this.getListWithChild(id);
        if (listWithChildren === null) {
            throw new Error(`List with id ${id} not found.`);
        }
        listWithChildren.children = await this.getChildrenRecursive(listWithChildren.children);
        return listWithChildren;
    };

    async getListWithChild(id: string) {
        return await prisma.list.findUnique({
            where: { id },
            include: { 
                children: true, 
                tags: true 
            }
        });
    }

    async getChildrenRecursive(children: List[]) {
        if (children.length === 0) {
            return [];
        }
        const allChildren = await Promise.all(children.map(async (child: List) => {
            const childWithChildren = await this.getListWithChild(child.id);
            if (childWithChildren === null) {
                throw new Error(`Child with id ${child.id} not found.`);
            }
            childWithChildren.children = await this.getChildrenRecursive(childWithChildren.children);
            return childWithChildren;
        }));
        return allChildren;
    }


    async findLists(filters: {
        parentId?: string;
        title?: string;
        isDone?: boolean;
        color?: string;
        tags?: string[]
    }): Promise<List[]> {
        const where: Prisma.ListWhereInput = {};

        if (filters.title) {
            where.title = { contains: filters.title, mode: 'insensitive' };
        }
        if (filters.isDone !== undefined) {
            where['isDone'] = filters.isDone;
        }
        if (filters.color) {
            where['color'] = filters.color;
        }
        // if (filters.tags?.length) {
        //     where['tags'] = { hasSome: filters.tags };
        // }
        if (filters.parentId !== undefined) {
            where['parentId'] = filters.parentId === 'null' ? null : filters.parentId;
        }

        return await prisma.list.findMany({
            where: where,
        });
    }


    async createList(
        title: string,
        isDone: boolean | undefined,
        color: string | undefined,
        tags: Tag[] | undefined,
        parentId: string | undefined,
        presetDate: string | undefined,
        position: number
    ) {
        // При создании элемента, который не является последним, сначала обновляем позиции последующих элементов
        await prisma.list.updateMany({
            where: {
                parentId,
                position: { gte: position }
            },
            data: {
                position: { increment: 1 } // Сдвигаем позиции последующих элементов на один вперёд
            }
        });

        // Создаём новый элемент с указанной позицией
        const createdList = await prisma.list.create({
            data: {
                title,
                isDone,
                color,
                // tags,
                parentId,
                presetDate,
                position
            }
        });
        return createdList;
    };



    async updateList(
        id: string,
        title: string | undefined,
        isDone: boolean | undefined,
        color: string | undefined,
        parentId: string | undefined,
        presetDate: string | undefined,
        position: number | undefined
    ) {
        const list = await prisma.list.findUnique({
            where: { id },
            include: { children: true }
        });

        if (!list) {
            throw new Error(`List with id ${id} not found.`);
        }

        // Рекурсивно обновляем статус isDone для всех дочерних элементов
        if (isDone !== undefined) {
            await this.updateChildrenIsDone(list.children, isDone);
        }

        const updatedList = await prisma.list.update({
            where: { id },
            data: {
                title,
                isDone,
                color,
                parentId,
                presetDate,
                position
            },
        });

        // Проверяем, нужно ли обновить статус isDone для родительских элементов
        if (list.parentId !== null && isDone !== undefined) {
            await this.updateParentIsDone(list.parentId, isDone);
        }

        return updatedList;
    }

    async updateChildrenIsDone(children: List[], isDone: boolean) {
        if (children.length === 0) {
            return;
        }

        for (const child of children) {
            await prisma.list.update({
                where: { id: child.id },
                data: { isDone }
            });

            const nestedChildren = await prisma.list.findMany({
                where: { parentId: child.id }
            });

            await this.updateChildrenIsDone(nestedChildren, isDone);
        }
    }

    async updateParentIsDone(parentId: string, isDone: boolean) {
        const parentList = await prisma.list.findUnique({
            where: { id: parentId },
            include: { children: true }
        });

        if (!parentList) {
            throw new Error(`List with id ${parentId} not found.`);
        }

        // Проверяем, все ли дочерние элементы родительского элемента имеют isDone: true
        const allChildrenDone = parentList.children.every(child => child.isDone);

        // Обновляем статус родительского элемента
        if (isDone) {
            // Если статус дочернего элемента изменен на isDone: true,
            // обновляем статус родительского элемента, если все дочерние элементы isDone: true
            if (allChildrenDone) {
                await prisma.list.update({
                    where: { id: parentId },
                    data: { isDone }
                });

                // Рекурсивно вызываем эту же функцию для обновления статуса родительского элемента родительского элемента
                if (parentList.parentId !== null) {
                    await this.updateParentIsDone(parentList.parentId, isDone);
                }
            }
        } else {
            // Если статус дочернего элемента изменен на isDone: false,
            // обновляем статус родительского элемента на isDone: false
            await prisma.list.update({
                where: { id: parentId },
                data: { isDone }
            });

            // Рекурсивно вызываем эту же функцию для обновления статуса родительского элемента родительского элемента
            if (parentList.parentId !== null) {
                await this.updateParentIsDone(parentList.parentId, isDone);
            }
        }
    }


    async deleteListWithChildren(id: string) {
        await this.deleteChildrenLists(id);
        await prisma.list.delete({
            where: { id }
        });
    }

    async deleteChildrenLists(parentId: string) {
        const childrenLists = await prisma.list.findMany({
            where: { parentId }
        });

        for (const childList of childrenLists) {
            await this.deleteChildrenLists(childList.id);
            await prisma.list.delete({
                where: { id: childList.id }
            });
        }
    }


    async getParentRecursive(id: string): Promise<List[]> {
        const list = await prisma.list.findUnique({
            where: { id },
            include: { parent: true }
        });

        if (!list) {
            throw new Error(`List with id ${id} not found.`);
        }

        const parentList = list.parent;
        if (!parentList) {
            return [];
        }

        const parentLists: List[] = [parentList];
        const grandParentLists = await this.getParentRecursive(parentList.id);
        return parentLists.concat(grandParentLists);
    }

    
    async addTagToList(listId: string, tagId: string): Promise<List> {
        const updatedList = await prisma.list.update({
            where: { id: listId },
            data: {
                tags: {
                    connect: { id: tagId }
                }
            },
            include: { tags: true }
        });

        return updatedList;
    }

    async removeTagFromList(listId: string, tagId: string): Promise<List> {
        const updatedList = await prisma.list.update({
            where: { id: listId },
            data: {
                tags: {
                    disconnect: { id: tagId }
                }
            },
            include: { tags: true }
        });

        return updatedList;
    }

    async getTagsByListId(listId: string): Promise<Tag[]> {
        const list = await prisma.list.findUnique({
            where: { id: listId },
            include: { tags: true }
        });

        if (!list) {
            throw new Error(`List with id ${listId} not found.`);
        }

        return list.tags;
    }

    async duplicateList(id: string, parentId?: string | null): Promise<List> {
        const originalList = await prisma.list.findUnique({
            where: { id },
            include: {
                children: true,
                tags: true
            }
        });

        if (!originalList) {
            throw new Error(`List with id ${id} not found.`);
        }

        const duplicatedList = await prisma.list.create({
            data: {
                title: originalList.title,
                isDone: originalList.isDone,
                color: originalList.color,
                tags: {
                    connect: originalList.tags.map(tag => ({ id: tag.id }))
                },
                parentId: parentId !== undefined ? parentId : originalList.parentId,
                presetDate: originalList.presetDate,
                position: originalList.position
            }
        });

        if (originalList.children.length > 0) {
            await Promise.all(originalList.children.map(async (child) => {
                await this.duplicateList(child.id, duplicatedList.id);
            }));
        }

        return duplicatedList;
    }



};

export default new ListService();
