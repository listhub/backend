import { Request, Response, NextFunction } from 'express';
import tagService from '../services/tag.service';

class TagsController {
    async createTag(request: Request, response: Response, next: NextFunction) {
        const tagData = request.body;
        try {
            const createdTag = await tagService.createTag(tagData);
            response.json(createdTag);
        } catch (error) {
            next(error);
            //   response.status(500).json({ message: error.message });
        }
    }

    async getTag(request: Request, response: Response, next: NextFunction) {
        const tagId = request.params.id;
        try {
            const tag = await tagService.getTagById(tagId);
            if (!tag) {
                return response.status(404).json({ message: 'Tag not found' });
            }
            response.json(tag);
        } catch (error) {
            next(error);
        }
    }

    async getAllTags(request: Request, response: Response, next: NextFunction) {
        try {
            const tags = await tagService.getAllTags();
            response.json(tags);
        } catch (error) {
            next(error);
        }
    }

    async updateTag(request: Request, response: Response, next: NextFunction) {
        const { id } = request.params;
        const tagData = request.body;
        try {
            const updatedTag = await tagService.updateTag(id, tagData);
            if (!updatedTag) {
                return response.status(404).json({ message: 'Tag not found' });
            }
            response.json(updatedTag);
        } catch (error) {
            next(error);
        }
    }

    async deleteTag(request: Request, response: Response, next: NextFunction) {
        const { id } = request.params;
        try {
            const deletedTag = await tagService.deleteTag(id);
            if (!deletedTag) {
                return response.status(404).json({ message: 'Tag not found' });
            }
            response.json(deletedTag);
        } catch (error) {
            next(error);
        }
    }
};

export default new TagsController();
