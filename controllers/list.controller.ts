import { Request, Response, NextFunction } from 'express';
import listService from '../services/list.service';
import tagService from '../services/tag.service';
import { Tag } from '@prisma/client';

class ListsController {

    async getListWithChildren(request: Request, response: Response, next: NextFunction) {
        try {
            const { id } = request.params;
            const { parentId, title, isDone, color, tags }: {
                parentId?: string;
                title?: string;
                isDone?: boolean | string;
                color?: string;
                tags?: Tag[]
            } = request.query;

            if (id) {
                const listWithChildren = await listService.getListRecursive(id);
                return response.json(listWithChildren);
            } else {
                const isDoneBoolean = isDone === 'true' ? true : false;
                const lists = await listService.findLists({ parentId, title, isDone: isDoneBoolean, color });
                return response.json(lists);
            }
        } catch (error) {
            next(error);
        }
    };

    async createList(request: Request, response: Response, next: NextFunction) {
        try {

            const { title, isDone, color, tags, parentId, presetDate, position }: {
                title: string;
                parentId: string;
                isDone: boolean;
                color: string | undefined;
                tags: Tag[] | undefined;
                presetDate: string | undefined;
                position: number;
            } = request.body;

            const listResponse = await listService.createList(
                title,
                isDone,
                color,
                tags,
                parentId,
                presetDate,
                position,
            );

            return response.json(listResponse);

        } catch (error) {
            next(error);
        }
    };

    async updateList(request: Request, response: Response, next: NextFunction) {
        try {
            const { id } = request.params;

            const { title, isDone, color, parentId, presetDate, position }: {
                title: string;
                isDone: boolean | undefined;
                color: string | undefined;
                parentId: string | undefined;
                presetDate: string | undefined;
                position: number | undefined;
            } = request.body;

            const updatedList = await listService.updateList(id, title, isDone, color, parentId, presetDate, position);

            return response.json(updatedList);
        } catch (error) {
            next(error);
        }
    }

    async deleteListWithChildren(request: Request, response: Response, next: NextFunction) {
        try {
            const { id } = request.params;
            await listService.deleteListWithChildren(id);
            return response.status(204).send();
        } catch (error) {
            next(error);
        }
    }

    async getParentList(request: Request, response: Response, next: NextFunction) {
        try {
            const { id } = request.params;

            const parentLists = await listService.getParentRecursive(id);

            return response.json(parentLists);
        } catch (error) {
            next(error);
        }
    }

    
    async addTagToList(request: Request, response: Response, next: NextFunction) {
        const { listId, tagId } = request.params;

        try {
            const list = await listService.getListWithChild(listId);
            const tag = await tagService.getTagById(tagId);

            if (!list || !tag) {
                return response.status(404).json({ message: 'List or Tag not found' });
            }

            const updatedList = await listService.addTagToList(listId, tagId);

            return response.json(updatedList);
        } catch (error) {
            next(error);
        }
    }

    async removeTagFromList(request: Request, response: Response, next: NextFunction) {
        const { listId, tagId } = request.params;
    
        try {
            const list = await listService.getListWithChild(listId);
            const tag = await tagService.getTagById(tagId);
    
            if (!list || !tag) {
                return response.status(404).json({ message: 'List or Tag not found' });
            }
    
            const updatedList = await listService.removeTagFromList(listId, tagId);
    
            const otherLists = await tagService.getListsByTagId(tagId);
            if (otherLists.length === 0) {
                await tagService.deleteTag(tagId);
            }
    
            return response.json(updatedList);
        } catch (error) {
            next(error);
        }
    }

    async getTagsByListId(request: Request, response: Response, next: NextFunction) {
        try {
            const { id } = request.params;
            const tags = await listService.getTagsByListId(id);
            response.json(tags);
        } catch (error) {
            next(error);
        }
    }

    async duplicateList(request: Request, response: Response, next: NextFunction) {
        try {
            const { id } = request.params;
            const { parentId } = request.body;
            const duplicatedList = await listService.duplicateList(id, parentId);
            return response.json(duplicatedList);
        } catch (error) {
            next(error);
        }
    };


}

export default new ListsController();
