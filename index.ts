import express from "express";
import { PrismaClient } from "@prisma/client";
import cors from "cors";
import router from "./router";

const PORT = process.env.PORT || 5000;

export const prisma = new PrismaClient();

const app = express();
app.use(express.json());
app.use(cors({
    credentials: true,
    origin: process.env.CLIENT_URL
}));
app.use('/api', router);

const start = async () => {
    try {
        app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
    } catch (e) {
        console.log(e);
        await prisma.$disconnect();
        process.exit(1);
    }
};

start();