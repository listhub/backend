import {Router} from "express";
import listController from "../controllers/list.controller";
import tagController from "../controllers/tag.controller";

const router = Router();

// Endpoints for list operations
router.route('/list')
    .post(listController.createList)
    .get(listController.getListWithChildren);

router.route('/list/:id')
    .get(listController.getListWithChildren)
    .put(listController.updateList)
    .delete(listController.deleteListWithChildren);

router.get('/list/:id/parents', listController.getParentList);
router.get('/list/:id/tags', listController.getTagsByListId);
router.post('/list/:listId/tag/:tagId', listController.addTagToList);
router.delete('/list/:listId/tag/:tagId', listController.removeTagFromList);
router.post('/list/:id/duplicate', listController.duplicateList);

// Endpoints for tag operations
router.route('/tag')
    .post(tagController.createTag)
    .get(tagController.getAllTags);

router.route('/tag/:id')
    .get(tagController.getTag)
    .put(tagController.updateTag)
    .delete(tagController.deleteTag);

export default router;
                    
