FROM node:latest

WORKDIR /app

COPY . .

RUN npm install
RUN npx prisma generate


EXPOSE 5000
CMD ["npm", "run", "dev", "--", "--host", "0.0.0.0", "--port", "5000"]