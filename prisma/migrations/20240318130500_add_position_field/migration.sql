/*
  Warnings:

  - Made the column `position` on table `List` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
CREATE SEQUENCE list_position_seq;
ALTER TABLE "List" ALTER COLUMN "position" SET NOT NULL,
ALTER COLUMN "position" SET DEFAULT nextval('list_position_seq');
ALTER SEQUENCE list_position_seq OWNED BY "List"."position";
