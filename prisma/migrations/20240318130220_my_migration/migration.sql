-- CreateTable
CREATE TABLE "List" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "isDone" BOOLEAN NOT NULL DEFAULT false,
    "color" TEXT,
    "tags" TEXT[],
    "createDate" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updateDate" TIMESTAMP(3) NOT NULL,
    "presetDate" TIMESTAMP(3),
    "parentId" TEXT,
    "position" INTEGER,
    "fulltextFields" TEXT[] DEFAULT ARRAY['title', 'tags']::TEXT[],

    CONSTRAINT "List_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "List" ADD CONSTRAINT "List_parentId_fkey" FOREIGN KEY ("parentId") REFERENCES "List"("id") ON DELETE SET NULL ON UPDATE CASCADE;
